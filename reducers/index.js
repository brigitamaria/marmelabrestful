import { combineReducers } from 'redux'
import times from './times'

const timeShow = combineReducers({
  times,
})

export default timeShow
