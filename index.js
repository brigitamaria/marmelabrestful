import React from 'react'
import ReactDOM from 'react-dom'
//import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import App from './modules/App'
// import City from './modules/City'
// import Welcome from './modules/Welcome'
// import { createStore } from 'redux'
// import reducer from './reducers'
// const store = createStore(reducer)

// <Router history={browserHistory}>
//     <Route path="/" component={App}>
//       <IndexRoute component={Welcome} />
//       <Route path="/Jakarta" component={() => (<City store={store} city='Jakarta'/>)}/>
//       <Route path="/Bandung" component={() => (<City store={store} city='Bandung'/>)}/>
//       <Route path="/Surabaya" component={() => (<City store={store} city='Surabaya'/>)}/>
//     </Route>
//   </Router>, document.getElementById('app'))

const render = () => ReactDOM.render(
  <BrowserRouter>
    <Route path="/" component={App}/>      
  </BrowserRouter>, document.getElementById('app'))

render()
store.subscribe(render)
