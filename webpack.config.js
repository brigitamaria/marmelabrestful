module.exports = {
  devtool:'eval',
  entry: './index.js',

  output: {
    filename: 'bundle.js',
    publicPath: ''
  },

  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader?presets[]=es2015&presets[]=react' }
    ]
  },
  node: {
  console: true,
  fs: 'empty',
  net: 'empty',
  tls: 'empty'
  }
}
