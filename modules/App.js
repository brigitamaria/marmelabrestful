import React from 'react'
import NavLink from './NavLink'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import City from './City'
import Welcome from './Welcome'
import { createStore } from 'redux'
import reducer from '../reducers'
const store = createStore(reducer)

export default React.createClass({
  render() {
    return (
      <div>
        <h1>React Router Tutorial</h1>
        <ul role="nav">
          <li><NavLink to="/" onlyActiveOnIndex>Home</NavLink></li>
          <li><NavLink to="/Jakarta">Jakarta</NavLink></li>
          <li><NavLink to="/Surabaya">Surabaya</NavLink></li>
          <li><NavLink to="/Bandung">Bandung</NavLink></li>
        </ul>
        <Switch>
          <Route exact path="/Jakarta" component={() => (<City store={store} city='Jakarta'/>)}/>
          <Route exact path="/Bandung" component={() => (<City store={store} city='Bandung'/>)}/>
          <Route exact path="/Surabaya" component={() => (<City store={store} city='Surabaya'/>)}/>
          <Route exact path="/" component={Welcome} />
        </Switch>
      </div>
    )
  }
})

// <Route path="/Jakarta" component={() => (<City store={store} city='Jakarta'/>)}/>
//       <Route path="/Bandung" component={() => (<City store={store} city='Bandung'/>)}/>
//       <Route path="/Surabaya" component={() => (<City store={store} city='Surabaya'/>)}/>