import React from 'react'
import request from 'request'
import restful, { requestBackend } from 'restful.js'
import NavLink from './NavLink'

export default React.createClass({
  getInitialState() {
  	return this.state = {
  		value:'',
      city:'',
      hrs:''
  	};
  },
  showNow() {
  	this.props.store.dispatch({type:'now',text:'1497506400'})
    this.doSomething();
  },
  showNext() {
  	this.props.store.dispatch({type:'next'})
    this.doSomething();
  },
  componentDidMount() {
    const api = restful('http://api.openweathermap.org/data/2.5', requestBackend(request));
    var city_ = this.props.city;
    var jktWeatherCollection = api.all('forecast');
    var jktWeather = jktWeatherCollection.getAll({
      "q":city_,
      "appid":"ae3c2fa5c34506f983045810d96fe2fe"
    });
    jktWeather.then((response) => {
        const weatherEntities = response.body();
        var val = weatherEntities.data();
        var list = val.list;
        var leng = this.props.store.getState().times.length;
        if(this.props.store.getState().times[leng-1].type==='next') {
          var next = list[1];
          var hrs = next.dt;
          this.setState({hrs:hrs});
          this.props.store.dispatch({type:'next',text:hrs});
          next = JSON.stringify(next.weather);
          this.setState({value:next});
        } else {
          var now = list[0];
          var hrs = now.dt;  
          this.setState({hrs:hrs});
          this.props.store.dispatch({type:'now',text:hrs});
          now = JSON.stringify(now.weather)
          this.setState({value:now})
        }
        this.setState({city:this.props.city})
    });
  },
  doSomething() {
    const api = restful('http://api.openweathermap.org/data/2.5', requestBackend(request));
    var city_ = this.props.city;
    var jktWeatherCollection = api.all('forecast');
    var jktWeather = jktWeatherCollection.getAll({
      "q":city_,
      "appid":"ae3c2fa5c34506f983045810d96fe2fe"
    });
    jktWeather.then((response) => {
        const weatherEntities = response.body();
        var val = weatherEntities.data();
        var list = val.list;
        var leng = this.props.store.getState().times.length;
        if(this.props.store.getState().times[leng-1].type==='now') {
          var now = list[0];
          var hrs = now.dt;
          this.setState({hrs:hrs});
          this.props.store.dispatch({type:'now',text:hrs});
          now = JSON.stringify(now.weather);
          this.setState({value:now});
        } else {
          var next = list[1];
          var hrs = next.dt;  
          this.setState({hrs:hrs});
          this.props.store.dispatch({type:'next',text:hrs});
          next = JSON.stringify(next.weather)
          this.setState({value:next})
        }
        this.setState({city:this.props.city})
    });
  },
  // componentWillReceiveProps(nextProps) {
  // 	const api = restful('http://api.openweathermap.org/data/2.5', requestBackend(request));
  // 	var city_ = this.props.city;
  // 	var jktWeatherCollection = api.all('forecast');
  // 	var jktWeather = jktWeatherCollection.getAll({
  // 	  "q":city_,
  // 	  "appid":"ae3c2fa5c34506f983045810d96fe2fe"
  // 	});
  // 	jktWeather.then((response) => {
  // 	    const weatherEntities = response.body();
  // 	    var val = weatherEntities.data();
  // 	    var list = val.list;
  //       console.log(list[0],"this");
  //       var leng = this.props.store.getState().times.length;
  // 	    if(this.props.store.getState().times[leng-1].type==='now') {
  // 	    	var now = list[0];
  //         var hrs = now.dt;
  //         this.setState({hrs:hrs});
  //         // this.props.store.dispatch({hrs:hrs});
  // 		    now = JSON.stringify(now.weather);
  // 	  	  this.setState({value:now});
  // 	    } else {
  // 	    	var next = list[1];
  //         var hrs = next.dt;  
  //         this.setState({hrs:hrs});
  //         // this.props.store.dispatch({hrs:hrs});
  // 		    next = JSON.stringify(next.weather)
  // 		    this.setState({value:next})
  // 	    }
  //       this.setState({city:this.props.city})
  // 	});
  // },
  render() {
    return (
    	<div>
	        <button onClick={this.showNow}>Now</button>
	        <button onClick={this.showNext}>Next 3 hours</button>
    		<div>{this.state.city}</div>
        <div>{this.state.hrs}</div>
        <div>{this.state.value}</div>
    	</div>
    	)
  }
})