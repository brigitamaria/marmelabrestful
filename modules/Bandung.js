import React from 'react'
import request from 'request'
import restful, { requestBackend } from 'restful.js'

export default React.createClass({
  getInitialState() {
    return this.state = {
      value: '',
      // name:'',
      // country:'',
      // weather:[]
    };
  },
  componentDidMount() {
    const api = restful('http://api.openweathermap.org/data/2.5', requestBackend(request));

  var jktWeatherCollection = api.all('weather');
  var jktWeather = jktWeatherCollection.getAll({
    "q":"Bandung",
    "appid":"ae3c2fa5c34506f983045810d96fe2fe"
  });

  jktWeather.then((response) => {
      const weatherEntities = response.body();
      var val = weatherEntities.data();

      for(var key in val) {
        console.log(val.name);
        if (val.hasOwnProperty(key)) {
          var temp = val[key];
          this.setState({key,temp});
        }
      }
      val = JSON.stringify(val);
      this.setState({value:val});
  });
  },
  render() {
    return (
      <div>
        <div>{this.state.value}</div>
      </div>
      )
  }
})